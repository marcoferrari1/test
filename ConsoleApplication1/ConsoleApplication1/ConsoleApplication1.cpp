#include "pch.h"

#include <vector> // vector containers
#include <cmath> // mathematical library
#include <iostream> // for the use of 'cout'
#include <fstream> // file streams
#include <sstream> // string streams
#include <cstdlib> // standard library


#define M_PI 3.14159265358979323846264338327950288

struct ParticleNode {
	double x;
	double y;
	double z;
	double x_ref;
	double y_ref;
	double z_ref;
	double ux;
	double uy;
	double uz;
	double f_x; // are no necessary to inilizialize since will be rested when called
	double f_y;
	double f_z;
	double S; // area of the node
};

class Particle {
public:
	//public functions
	Particle(); // constructor forward declaration
	static Particle makeSphere(double radius, double x, double y, double z); // create the partcle in the shape of a sphere with radius and center at xyc
	static Particle makeSpherePolar(double radius, double x, double y, double z);
	// variables
	int numNodes;
	double radius;
	ParticleNode bodyCenter;
	ParticleNode *node;

private:
	//private functions
};

Particle::Particle() {}

Particle Particle::makeSphere(double radius, double xc, double yc, double zc) {
	Particle result;
	int i, j;  // counters for loops
	double r_b; //correct sphere radius
	double r_n; // radius of sphere in the xy plane
	double z_s; // z difference from the center of the spehere of the strip
	double r;

	r = radius;
	result.radius = r;
	result.bodyCenter.x = xc;
	result.bodyCenter.y = yc;
	result.bodyCenter.z = zc;
	result.bodyCenter.x_ref = xc;
	result.bodyCenter.y_ref = yc;
	result.bodyCenter.z_ref = zc;

	//Calculate correct radius of sphere based on article
	r_b = pow(((pow(r, 3) + pow((r - 1.0), 3.0)) / 2.0), (1.0 / 3.0)); // corrected radius
	//r_b = 6;


	double node_lattice_distance_ratio = 1.0;
	//double spacing = 1.0 / node_lattice_distance_ratio;
	double spacing = 1.0;
	//calculate the number of strips
	int n_s = (int)(M_PI*r_b + 1);
	// define the number of nodes per strip
	int qNode[1000];
	double areaEsfera = 4 * M_PI*r_b*r_b;

	qNode[1 - 1] = 1;

	for (i = 1; i < n_s; i++) {  // for nodes from 1 to n_s-1
		// i is the strip number from south pole to north pole
		qNode[i] = (int)(2 * (double)(n_s - 1)*sin(M_PI * (double)(i) / (n_s - 1)));
	}
	qNode[n_s - 1] = 1;
	//calculate the total number of nodes
	result.numNodes = 0;
	for (i = 0; i < n_s; i++) {
		result.numNodes = result.numNodes + qNode[i];
	}

	result.node = new ParticleNode[result.numNodes];

	//south node
	result.node[0].x = result.bodyCenter.x;
	result.node[0].y = result.bodyCenter.y;
	result.node[0].z = result.bodyCenter.z - r_b;
	result.node[0].x_ref = result.bodyCenter.x;
	result.node[0].y_ref = result.bodyCenter.y;
	result.node[0].z_ref = result.bodyCenter.z - r_b;
	result.node[0].ux = 0;
	result.node[0].uy = 0;
	result.node[0].uz = 0;
	result.node[0].S = M_PI * M_PI*r_b*r_b / ((double)(n_s - 1)*(double)(n_s - 1));

	areaEsfera -= result.node[0].S;

	//intermediary strips

	int nodeIndex = 0; // just to increment the index of the node
	for (i = 1; i < n_s; i++) {
		//calculate z_s and r_n  // i is strip index
		r_n = r_b * sin(M_PI*i / (n_s - 1));
		z_s = r_b * cos(M_PI*i / (n_s - 1));

		for (j = 0; j < qNode[i]; j++) {

			nodeIndex = nodeIndex + 1;
			result.node[nodeIndex].z = result.bodyCenter.z - z_s;
			result.node[nodeIndex].y = result.bodyCenter.y + r_n * sin(j * 2 * M_PI / qNode[i]);
			result.node[nodeIndex].x = result.bodyCenter.x + r_n * cos(j * 2 * M_PI / qNode[i]);

			result.node[nodeIndex].z_ref = result.bodyCenter.z_ref - z_s;
			result.node[nodeIndex].y_ref = result.bodyCenter.y_ref + r_n * sin(j * 2 * M_PI / qNode[i]);
			result.node[nodeIndex].x_ref = result.bodyCenter.x_ref + r_n * cos(j * 2 * M_PI / qNode[i]);

			result.node[nodeIndex].ux = 0;
			result.node[nodeIndex].uy = 0;
			result.node[nodeIndex].uz = 0;

			//PAPPUS THEOREM
			result.node[nodeIndex].S = 2.0*M_PI*(M_PI*r_b / (double)n_s)*(r_n) / qNode[i];
			areaEsfera -= result.node[nodeIndex].S;

		}
	}
	//north pole
	result.node[nodeIndex].x = result.bodyCenter.x;
	result.node[nodeIndex].y = result.bodyCenter.y;
	result.node[nodeIndex].z = result.bodyCenter.z + r_b;
	result.node[nodeIndex].x_ref = result.bodyCenter.x_ref;
	result.node[nodeIndex].y_ref = result.bodyCenter.y_ref;
	result.node[nodeIndex].z_ref = result.bodyCenter.z_ref + r_b;
	result.node[nodeIndex].ux = 0;
	result.node[nodeIndex].uy = 0;
	result.node[nodeIndex].uz = 0;
	result.node[nodeIndex].S = M_PI * M_PI*r_b*r_b / ((double)(n_s - 1)*(double)(n_s - 1));

	areaEsfera -= result.node[nodeIndex].S;
	return result;
}
Particle Particle::makeSpherePolar(double diameter, double xc, double yc, double zc) {
	Particle result;
	int n_s, qtdNos[1000];
	int i, j;
	double theta[1000], zeta[1000], S[1000];
	double r;
	double phase = 0;
	double scale = 1.0; // min distance between each node

	//define the properties of the particle
	r = diameter / 2;
	result.radius = r;
	result.bodyCenter.x = xc;
	result.bodyCenter.y = yc;
	result.bodyCenter.z = zc;
	result.bodyCenter.x_ref = xc;
	result.bodyCenter.y_ref = yc;
	result.bodyCenter.z_ref = zc;
	result.bodyCenter.S = 4.0*M_PI*r*r;
	result.bodyCenter.ux = 0;
	result.bodyCenter.uy = 0;
	result.bodyCenter.uz = 0;

	//calculate the number of layer the sphere will have
	n_s = (int)(2.0 *sqrt(2)*r / scale + 1.0);


	result.numNodes = 0;
	for (i = 0; i <= n_s; i++) {
		theta[i] = M_PI * ((double)i / (double)n_s - 0.5); // determine the angle of each layer
		qtdNos[i] = (int)(1.5 + cos(theta[i]) * n_s*sqrt(3)); // determine the number of node per layer
		result.numNodes = result.numNodes + qtdNos[i]; //calcaulate the total number of notes on the sphere
		zeta[i] = r * sin(theta[i]); // determine the height of each layer
	}


	for (i = 0; i < n_s; i++) {
		S[i] = (zeta[i] + zeta[i + 1]) / 2.0 - zeta[0]; // calculate the distance to the south pole to the mid distance of the layer and previous layer
	}
	S[n_s] = 2 * r;
	for (i = 0; i <= n_s; i++) {
		S[i] = 2 * M_PI*r*S[i]; // calculate the area of sphere segment since the south pole
	}
	for (i = n_s; i > 0; i--) {
		S[i] = S[i] - S[i - 1]; //calculate the area of the layer
	}
	S[0] = S[n_s];



	result.node = new ParticleNode[result.numNodes];

	//south node - define all properties
	result.node[0].x = xc;
	result.node[0].y = yc;
	result.node[0].z = zc + r * sin(theta[0]);
	result.node[0].x_ref = xc;
	result.node[0].y_ref = yc;
	result.node[0].z_ref = zc + r * sin(theta[0]);
	result.node[0].ux = 0;
	result.node[0].uy = 0;
	result.node[0].uz = 0;
	result.node[0].S = S[0];


	int nodeIndex = 0;
	for (i = 1; i < n_s; i++) {
		if (i % 2 == 1) {
			phase = phase + M_PI / qtdNos[i]; // calculate the phase of the segmente to avoid a straight point line
		}
		else {
			phase = phase + 0;
		}
		for (j = 0; j < qtdNos[i]; j++) {
			// determine the properties of each node in the mid layers
			nodeIndex = nodeIndex + 1;
			result.node[nodeIndex].x = xc + r * cos(theta[i])*cos(j * 2 * M_PI / qtdNos[i] + phase);
			result.node[nodeIndex].y = yc + r * cos(theta[i])*sin(j * 2 * M_PI / qtdNos[i] + phase);
			result.node[nodeIndex].z = zc + r * sin(theta[i]);

			result.node[nodeIndex].z_ref = result.node[nodeIndex].x;
			result.node[nodeIndex].y_ref = result.node[nodeIndex].y;
			result.node[nodeIndex].x_ref = result.node[nodeIndex].z;

			result.node[nodeIndex].ux = 0;
			result.node[nodeIndex].uy = 0;
			result.node[nodeIndex].uz = 0;
			// the area of sphere segment is divided by the number of node in the layer, so all nodes have the same area
			result.node[nodeIndex].S = S[i] / qtdNos[i];

		}
	}
	nodeIndex = nodeIndex + 1;
	//north pole -define all properties


	result.node[nodeIndex].x = xc;
	result.node[nodeIndex].y = yc;
	result.node[nodeIndex].z = zc + r * sin(theta[n_s]);
	result.node[nodeIndex].x_ref = xc;
	result.node[nodeIndex].y_ref = yc;
	result.node[nodeIndex].z_ref = zc + r * sin(theta[n_s]);
	result.node[nodeIndex].ux = 0;
	result.node[nodeIndex].uy = 0;
	result.node[nodeIndex].uz = 0;
	result.node[nodeIndex].S = S[n_s];

	return result;
}
int main() {
	int x;
	Particle sphere1 = Particle::makeSpherePolar(10, 0.0, 0.0, 0.0);


	x = sphere1.numNodes;

	for (int i = 0; i < sphere1.numNodes; i++) {
		printf("%e %e %e\n", sphere1.node[i].x, sphere1.node[i].y, sphere1.node[i].z);
	}
	return 0;
}
